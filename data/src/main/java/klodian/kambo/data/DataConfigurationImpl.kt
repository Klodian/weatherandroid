package klodian.kambo.data

class DataConfigurationImpl : DataConfiguration {
    override val appIdValue = "54f39321bc1d3832e14e73a481b46aa8"
    override val appIdParam = "appid"
    override val baseUrl = "https://api.openweathermap.org"
}