package klodian.kambo.data

interface DataConfiguration {
    val appIdValue: String
    val appIdParam: String
    val baseUrl: String
}