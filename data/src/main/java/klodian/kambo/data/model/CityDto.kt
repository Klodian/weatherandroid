package klodian.kambo.data.model

import java.util.*

data class CityDto(
    val name: String,
    val country: String,
    val timezone: Int
)